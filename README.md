**Fediverse sticker archive**
-

The Fediverse sticker-set-01 was originally created for [**Signal**](https://www.signal.org) messenger.

To add the sticker-set-01 to your signal messenger use this [**direct signal link**](https://signal.art/addstickers/#pack_id=09550c23d5659da398ad2b8aca4f8a58&pack_key=ed09edc24cdb7f37a3a94893f6e5c79bbabeb1511f221927341f3d411f0f7729).

The link works with the signal app on your smartphone or desktop version.

Someone also made them available on [**signalstickers.com**](https://signalstickers.com/pack/09550c23d5659da398ad2b8aca4f8a58)

First published: https://chaos.social/@moagee/105701915311114804

... and what is the [**Fediverse**](https://en.wikipedia.org/wiki/Fediverse)?

**info:** the logos of the sticker-set-01 are not originally created by moagee, they are just a collection of logos from the fediverse and their corresponding websites and are a replica of these original files. if you want your logo to be deleted, please contact me: moagee (at) mailbox.org or https://chaos.social/@moagee
